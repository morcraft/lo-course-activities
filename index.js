const _ = require('lodash')
const buildLink = require('./buildLink.js')
const sort = require('./sort.js')
const isLocal = require('./isLocal.js')
const getBasePath = require('./getBasePath.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Data was expected in instance in an object.')

    if(!_.isObject(args.data))
        throw new Error('Data was expected in a "data" property in object.') 
        
    var self = this
    self.data = args.data 
    if(isLocal()){
        if(!_.isString(args.remoteURL))
            throw new Error('Invalid remoteURL')
        self.host = args.remoteURL
    }
    else{
        self.host = getBasePath()
    }

    if(args.sort !== false){
        self.data = self.sort({
            data: args.data,
            host: self.host
        })
    }

    return self
}

module.exports.prototype.buildLink = buildLink
module.exports.prototype.sort = sort   