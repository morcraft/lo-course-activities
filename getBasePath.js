const _ = require('lodash')
const assertLocation = require('./assertLocation.js')

module.exports = function(){
    assertLocation()
    if(location.pathname.indexOf('extension') > -1)
        return location.origin + '/extension'

    return location.origin
}