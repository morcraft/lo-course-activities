const _ = require('lodash')
const buildLink = require('./buildLink.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Some arguments were expected in an object in method.')
  
    if(!_.isObject(args.data))
        throw new Error('Arguments were expected in a "data" property.')

    if(!_.isString(args.host))
        throw new Error('A host was expected in a string.')

    var data = {}
    _.forEach(args.data, function(v, k){
        if(!_.isUndefined(v.deletioninprogress))
            return true  

        if(!_.isObject(data[v.mod]))
            data[v.mod] = {}

        var nextIndex = _.size(data[v.mod])

        data[v.mod][nextIndex] = _.merge({}, v, {
            href: buildLink({
                data: v,
                host: args.host
            })
        })
    })

    return data
}