const _ = require('lodash')

module.exports =  function(){
    if(!_.isObject(window))
        throw new Error('Non-browser environment?')

    if (!_.isObject(window.location))
        throw new Error('Environment seems to be wrong. Couldn\'t find location.')
}